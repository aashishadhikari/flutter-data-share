import 'dart:io' show Platform;

import 'package:flutter/services.dart';

class SharedData {
  final String androidPackageName;
  final String iosSuitName;

  SharedData({
    required this.androidPackageName,
    required this.iosSuitName,
  });

  static const MethodChannel _channel = const MethodChannel('shared_data');

  Future<bool?> setString(String key, String value) async {
    Map<String, dynamic> arguments = {
      "package": Platform.isAndroid ? androidPackageName : iosSuitName,
      "key": key,
      "value": value,
    };

    return _channel.invokeMethod('save', arguments);
  }

  Future<String?> getString(String key) async {
    Map<String, dynamic> arguments = {
      "package": Platform.isAndroid ? androidPackageName : iosSuitName,
      "key": key,
    };

    return _channel.invokeMethod('read', arguments);
  }

  ///Need to specify [key] for iOS, not required for Android
  Future<bool?> clearData({String? key}) async {
    if (Platform.isIOS && (key ?? '').isEmpty) {
      throw ('Please specify key for iOS');
    }
    Map<String, dynamic> arguments = {
      "package": Platform.isAndroid ? androidPackageName : iosSuitName,
      "key": key,
    };

    return _channel.invokeMethod('clear', arguments);
  }
}
