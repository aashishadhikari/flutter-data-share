package com.newitventure.shared_data
import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManager.VISIBILITY_USER_MANAGED_VISIBLE
import android.os.Build
import android.util.Log
import androidx.annotation.NonNull
import android.content.Context
import androidx.annotation.RequiresApi

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar


/** SharedDataPlugin */
class SharedDataPlugin : FlutterPlugin, MethodCallHandler {
    private var accType = "com.nitv.bnpj.account.AUTH"
    private val account = Account("BNPJ", accType)
    private lateinit var context: Context

  companion object {
    const val CHANNEL_NAME = "shared_data"
  


    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), CHANNEL_NAME)
      channel.setMethodCallHandler(SharedDataPlugin())
    }
  }

  private lateinit var channel: MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, CHANNEL_NAME)
    context = flutterPluginBinding.applicationContext
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "save" -> {
        try{
          var key = call.argument<String>("key").toString()
          var value = call.argument <String>("value").toString()
          result.success(setData(key,value))
        }
        catch (e:Exception){
          result.success(false) }
      }
      "read" -> {
        var key = call.argument<String>("key").toString()
        result.success(getData(key))
      }
      "clear" -> {        
        result.success(clearData())
      }
      else -> result.notImplemented()
    }
  }
  fun getData(key:String):String? {
    val accountManager = AccountManager.get(context) 
    return accountManager.peekAuthToken(account, key)
  }


    @RequiresApi(Build.VERSION_CODES.O)
    fun clearData() : Boolean  {
      val accountManager = AccountManager.get(context)
      return try {
        accountManager.removeAccountExplicitly(account)

      } catch (e: Exception) {
        Log.d("acx", e.toString())
        false
      }
  
    }


  @RequiresApi(Build.VERSION_CODES.O)
  fun setData(key:String, value: String) : Boolean  {
    val accountManager = AccountManager.get(context) //this is Activity

    if (accountManager.getAccountsByType(accType).isEmpty()) {
      try {
        val success = accountManager.addAccountExplicitly(account, "", null)
        // Log.d("acx", "Account created")
        if (success) {
        } else {
          // Log.d("acx", "Account creation failed. Look at previous logs to investigate")
          return false
        }
      } catch (e: Exception) {
        Log.d("acx", e.toString())
        return false
      }
    }
    accountManager.setAccountVisibility(account, accType, AccountManager.VISIBILITY_USER_MANAGED_VISIBLE)
    accountManager.setAuthToken(account, key, value)
    return true
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

}