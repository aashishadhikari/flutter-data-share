import Flutter
import Foundation

public class SwiftSharedDataPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "shared_data", binaryMessenger: registrar.messenger())
        let instance = SwiftSharedDataPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if call.method == "save" {
            save(arguments: call.arguments, result: result)
        } else if call.method == "read" {
            read(arguments: call.arguments, result: result)
        } else if call.method == "clear" {
            clear(arguments: call.arguments, result: result)
        } else {
            result(FlutterMethodNotImplemented)
        }
    }
    
    public func save(arguments: Any?, result: @escaping FlutterResult){
        
        let argumentsMap = arguments as! NSDictionary

        let suiteName = argumentsMap.value(forKey: "package") as! String
        let key = argumentsMap.value(forKey: "key") as! String
        let value = argumentsMap.value(forKey: "value") as! String
        
        let userDefaults = UserDefaults(suiteName: suiteName)
        do{
            userDefaults?.set(value, forKey: key)
           result(true)
        }catch{
            result(false)
        }
    }
    
    public func read(arguments: Any?, result: @escaping FlutterResult){
        
        let argumentsMap = arguments as! NSDictionary
        
        let suiteName = argumentsMap.value(forKey: "package") as! String
        let key = argumentsMap.value(forKey: "key") as! String
        
        let userDefaults = UserDefaults(suiteName: suiteName)
        
        let data = userDefaults?.string(forKey: key)
        result(data)
    }

     public func clear(arguments: Any?, result: @escaping FlutterResult){
        
        let argumentsMap = arguments as! NSDictionary
        
        let suiteName = argumentsMap.value(forKey: "package") as! String
        let key = argumentsMap.value(forKey: "key") as! String
        
        let userDefaults = UserDefaults(suiteName: suiteName)
        
        userDefaults?.removeObject(forKey: key)
        result(true)
    }
}
