#import "SharedDataPlugin.h"
#if __has_include(<shared_data/shared_data-Swift.h>)
#import <shared_data/shared_data-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "shared_data-Swift.h"
#endif

@implementation SharedDataPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftSharedDataPlugin registerWithRegistrar:registrar];
}
@end
